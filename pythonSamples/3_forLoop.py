friends = ['john', 'pat', 'gary', 'george']
for i, name in enumerate(friends):
    print "iteration {iteration} is {name}".format(iteration=i, name=name)

# Can you make it skip the second name? how about names that start with 'g'?
