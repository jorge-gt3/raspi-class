def greet(name):
    print 'Hello', name

greet('Jack')
greet('Jill')
greet('Bob')

# Can you build an array of names and call the function for each one?
