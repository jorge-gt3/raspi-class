parents, babies = (0, 1)
while babies < 100:
    print 'This month has {0} pair of babies rabbits'.format(babies)
    parents, babies = (babies, parents + babies)

# Can you display how many adult couples are there in each month?
