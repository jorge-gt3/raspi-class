# we need to import OpenCV... can you find out how to install it? 
# tip... we need to use apt-get
import cv2

# create a variable with the camera you want to connect...
cap = cv2.VideoCapture('http://camera_url')

# create an endless loop that is reading frames from the camera 
while False:
  ret, frame = cap.read()
  cv2.imshow('Video', frame)

#stop the loop of a key is pressed
  if cv2.waitKey(1) == 27:
    exit(0)
